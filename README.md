# 桌面时钟 Lite for OpenHarmony

该项目是我练手鸿蒙开发的第一个项目，使用 ArkTS 开发。

## 特性

无

## 系列项目

- [桌面时钟 for Android](https://gitlab.com/Jesse205/Desk-Clock-Lite)
- [桌面时钟 for 浏览器](https://gitee.com/Jesse205/Jesse205/tree/master/deskclocklite)